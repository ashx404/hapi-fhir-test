FROM hapiproject/hapi:base as build-hapi


ARG HAPI_FHIR_STARTER_URL=https://gitlab.com/ashx404/hapi-fhir-test
ARG HAPI_FHIR_STARTER_BRANCH=master


# # RUN /tmp/apache-maven-3.6.2/bin/mvn install -DskipTests

WORKDIR /tmp
RUN git clone --branch ${HAPI_FHIR_STARTER_BRANCH} ${HAPI_FHIR_STARTER_URL}

WORKDIR /tmp/hapi-fhir-test
#RUN /tmp/apache-maven-3.6.2/bin/mvn dependency:resolve
RUN export MAVEN_OPTS="-Xmx512m"
RUN /tmp/apache-maven-3.6.2/bin/mvn clean install -DskipTests


FROM tomcat:9-jre11

RUN mkdir -p /data/hapi/lucenefiles && chmod 775 /data/hapi/lucenefiles
# VOLUME /tmp

COPY --from=build-hapi /tmp/hapi-fhir-test/target/hapi-fhir-jpaserver.war /usr/local/tomcat/webapps/

EXPOSE 8080

CMD ["catalina.sh", "run"]