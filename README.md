Clone the repo

```
git clone https://gitlab.com/ashx404/hapi-fhir-test.git
```

cd into folder

```
cd hapi-fhir-test
```

## Running instructions

The following command will build the war file from the src folder using maven-war-plugin

```
docker-compose up
```
